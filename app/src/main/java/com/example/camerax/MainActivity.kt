package com.example.camerax

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.media.MediaPlayer
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture.OnImageCapturedCallback
import androidx.camera.core.ImageProxy
import androidx.camera.view.CameraController
import androidx.camera.view.LifecycleCameraController
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material3.BottomSheetScaffold
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.rememberBottomSheetScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.camerax.ui.theme.CameraXTheme
import com.example.camerax.utils.components.CameraPreview
import com.example.camerax.utils.components.PhotoBottomSheetContent
import com.example.camerax.viewmodel.MainViewModel
import kotlinx.coroutines.launch
import java.io.IOException
import java.text.SimpleDateFormat

class MainActivity : ComponentActivity() {
    private var mediaPlayer: MediaPlayer? = null
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!allPermissionsGranted()) {
            ActivityCompat.requestPermissions(this, ALL_PERMISSIONS, 0)
        }

        setContent {
            CameraXTheme {
                val scope = rememberCoroutineScope()
                val scaffoldState = rememberBottomSheetScaffoldState()

                val viewModel = viewModel<MainViewModel>()
                val bitmaps by viewModel.bitmaps.collectAsState()

                val controller = remember{
                    LifecycleCameraController(applicationContext).apply {
                        setEnabledUseCases(CameraController.IMAGE_CAPTURE or CameraController.VIDEO_CAPTURE)
                    }
                }

                BottomSheetScaffold(
                    scaffoldState = scaffoldState,
                    sheetPeekHeight = 0.dp,
                    sheetContent = {
                        PhotoBottomSheetContent(
                            bitmaps = bitmaps,
                            modifier = Modifier
                                .fillMaxWidth()
                        )
                    }
                ) { padding ->
                    ScreenContent(
                        paddingValues = padding,
                        controller = controller,
                        block = {
                            scope.launch {
                                scaffoldState.bottomSheetState.expand()
                            }
                        },
                        onPhotoTaken = {
                            viewModel.onTakePhoto(it)
                        }
                    )
                }
            }
        }
    }

    @Composable
    private fun ScreenContent(paddingValues: PaddingValues, controller: LifecycleCameraController, block: () -> Unit, onPhotoTaken: (Bitmap) -> Unit) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues)
                .background(color = Color.Black)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.2F)
            ) {
                Spacer(modifier = Modifier.height(72.dp))
                CameraPreview(
                    controller = controller,
                    modifier = Modifier
                        .offset(0.dp, 72.dp)
                        .aspectRatio(3f / 4f),
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(0.1F)
                    .padding(horizontal = 16.dp, vertical = 48.dp),
                horizontalArrangement = Arrangement.SpaceAround,
                verticalAlignment = Alignment.Bottom
            ) {

                IconButton(
                    onClick = block
                ) {
                    Icon(
                        modifier = Modifier
                            .size(56.dp),
                        painter = painterResource(id = R.drawable.ic_gallery),
                        contentDescription = null,
                        tint = Color.White
                    )
                }

                IconButton(
                    onClick = {
                        playSound()

                        takePhoto(applicationContext, controller) { bitmap ->
                            val rotate = if (controller.cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA) 90F else -90F
                            savePhoto(bitmap = rotateBitmapIfNeeded(bitmap = bitmap, rotate = rotate))
                            onPhotoTaken.invoke(rotateBitmapIfNeeded(bitmap = bitmap, rotate = rotate))
                        }
                    }
                ) {
                    Icon(
                        modifier = Modifier
                            .size(100.dp),
                        painter = painterResource(id = R.drawable.ic_capture),
                        contentDescription = null,
                        tint = Color.White
                    )
                }

                IconButton(
                    onClick = {
                        controller.cameraSelector = if (controller.cameraSelector == CameraSelector.DEFAULT_BACK_CAMERA)
                            CameraSelector.DEFAULT_FRONT_CAMERA
                        else
                            CameraSelector.DEFAULT_BACK_CAMERA
                    }
                ) {
                    Icon(
                        modifier = Modifier
                            .size(56.dp),
                        painter = painterResource(id = R.drawable.ic_rep_camera),
                        contentDescription = null,
                        tint = Color.White
                    )
                }

            }
        }
    }

    private fun takePhoto(context: Context, controller: LifecycleCameraController, onPhotoTaken: (Bitmap) -> Unit) {
        controller.takePicture(ContextCompat.getMainExecutor(context), object : OnImageCapturedCallback() {
            override fun onCaptureSuccess(image: ImageProxy) {
                super.onCaptureSuccess(image)
                onPhotoTaken(image.toBitmap())
            }
        })
    }

    private fun playSound() {
        mediaPlayer = MediaPlayer.create(this, R.raw.camera_sound)
        mediaPlayer!!.start() // Starts playback

        mediaPlayer!!.setOnCompletionListener {
            it.release()
        }
    }

    @SuppressLint("SimpleDateFormat", "InlinedApi")
    private fun savePhoto(bitmap: Bitmap) {
        val name = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(System.currentTimeMillis())
        val displayName = "$name.jpg"

        val resolver = this.contentResolver
        val imageCollection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)

        val imageDetails = ContentValues().apply {
            put(MediaStore.Images.Media.DISPLAY_NAME, displayName)
            put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            put(MediaStore.Images.Media.WIDTH, bitmap.width)
            put(MediaStore.Images.Media.HEIGHT, bitmap.height)
        }

        val imageUri = resolver.insert(imageCollection, imageDetails)

        try {
            imageUri?.let { uri ->
                val outputStream = resolver.openOutputStream(uri)
                outputStream?.use { stream ->
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun rotateBitmapIfNeeded(bitmap: Bitmap, rotate: Float): Bitmap {
        val rotatedBitmap: Bitmap
        val matrix = Matrix()
        matrix.setRotate(rotate)
        rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        return rotatedBitmap
    }

//    private fun openGalleryForImage() {
//        val intent = Intent(Intent.ACTION_PICK)
//        intent.type = "image/*"
//        startActivityForResult(intent, REQUEST_CODE_PICK_IMAGE)
//    }

    companion object {
        @SuppressLint("InlinedApi")
        private val ALL_PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        private const val REQUEST_CODE_PICK_IMAGE = 100
    }

    private fun allPermissionsGranted(): Boolean =
        ALL_PERMISSIONS.all {
            ActivityCompat.checkSelfPermission(applicationContext, it) == PackageManager.PERMISSION_GRANTED
        }

}

